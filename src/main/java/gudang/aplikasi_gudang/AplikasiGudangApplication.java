package gudang.aplikasi_gudang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiGudangApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiGudangApplication.class, args);
	}

}
