package gudang.aplikasi_gudang.model.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import gudang.aplikasi_gudang.model.dto.KategoriItemDto;
import gudang.aplikasi_gudang.model.entity.KategoriItemEntity;
import gudang.aplikasi_gudang.model.repository.KategoryItemRepository;

@RestController
@RequestMapping("/kategori")
public class KategoriItemController {
private final KategoryItemRepository kategoryItemRepository;
	
	@Autowired
	public KategoriItemController(KategoryItemRepository kategoryItemRepository) {
		this.kategoryItemRepository = kategoryItemRepository;
	}

	@GetMapping
	public List<KategoriItemDto> get(){
		List<KategoriItemEntity> kategoriEntityList = kategoryItemRepository.findAll();
		List<KategoriItemDto> kategoriDtoList=kategoriEntityList.stream().map(this::convertToDto)
				.collect(Collectors.toList());
		return kategoriDtoList;
		
	}
	
	@GetMapping("/{id}")
	    public KategoriItemDto get(@PathVariable Integer id) {
	        if(kategoryItemRepository.findById(id).isPresent()){
	           KategoriItemDto kategoriItemDto = convertToDto(kategoryItemRepository.findById(id).get());
	            return kategoriItemDto;
	        }
	        return null;
	    }
	@PostMapping
	public KategoriItemDto insert(@RequestBody KategoriItemDto dto) {
		KategoriItemEntity kategoriItemEntity = convertToEntity(dto);
		kategoryItemRepository.save(kategoriItemEntity);
		return dto;
		
	}
	
	@PutMapping("/{id}")
	public KategoriItemDto update(@RequestBody KategoriItemDto newKategori, @PathVariable Integer id) {
		if(kategoryItemRepository.findById(id).isPresent()) {
			KategoriItemDto kategoriItemDto = convertToDto(kategoryItemRepository.findById(id).get());
			kategoriItemDto.setNamaKategoriDto(newKategori.getNamaKategoriDto());
			KategoriItemEntity kategoriItemEntity = new KategoriItemEntity();
			kategoriItemEntity = convertToEntity(kategoriItemDto);
			kategoryItemRepository.save(kategoriItemEntity);
			return kategoriItemDto;
			
		}
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Integer id) {
		kategoryItemRepository.deleteById(id);
	}
	
	private KategoriItemEntity convertToEntity(KategoriItemDto dto) {
		KategoriItemEntity kategoriItemEntity = new KategoriItemEntity();
		kategoriItemEntity.setIdKategoriEntity(dto.getIdKategoriDto());
		kategoriItemEntity.setNamaKategoriEntity(dto.getNamaKategoriDto());
		return kategoriItemEntity;
		
	}
	
	private KategoriItemDto convertToDto(KategoriItemEntity kategoriItemEntity) {
		KategoriItemDto kategoriItemDto = new KategoriItemDto();
		kategoriItemDto.setIdKategoriDto(kategoriItemEntity.getIdKategoriEntity());
		kategoriItemDto.setNamaKategoriDto(kategoriItemEntity.getNamaKategoriEntity());
		return kategoriItemDto;
		
	}
	
}
