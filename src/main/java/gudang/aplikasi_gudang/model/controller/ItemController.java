package gudang.aplikasi_gudang.model.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gudang.aplikasi_gudang.model.dto.ItemDto;
import gudang.aplikasi_gudang.model.entity.ItemEntity;
import gudang.aplikasi_gudang.model.entity.KategoriItemEntity;
import gudang.aplikasi_gudang.model.repository.ItemRepository;
import gudang.aplikasi_gudang.model.repository.KategoryItemRepository;

@RestController
@RequestMapping("/item")
public class ItemController {
	private final ItemRepository itemRepository;
	private final KategoryItemRepository kategoryItemRepository;
	
	@Autowired
	public ItemController(ItemRepository itemRepository, KategoryItemRepository kategoryItemRepository) {
		this.itemRepository = itemRepository;
		this.kategoryItemRepository = kategoryItemRepository;
	}

	@GetMapping
	public List<ItemDto> get(){
		List<ItemEntity> itemEntityList = itemRepository.findAll();
		List<ItemDto> itemDtoList= itemEntityList.stream().map(this::convertToDto).collect(Collectors.toList());
		return itemDtoList;
	}
	
	@GetMapping("/{id}")
	public ItemDto get(@PathVariable Integer id) {
		if(itemRepository.findById(id).isPresent()) {
			ItemDto itemDto = convertToDto(itemRepository.findById(id).get());
			return itemDto;
		}
		return null;
	}
	
	@GetMapping("/kateg/{idKategori}")
	public List<ItemDto> getByKategoriItemEntity(@PathVariable Integer idKategori){
		List<ItemEntity> itemEntityList = itemRepository.findAllByKategoriItemEntityIdKategori(idKategori);
		List<ItemDto> itemDtoList = itemEntityList.stream().map(this::convertToDto).collect(Collectors.toList());
		return itemDtoList;
	}
	
	@PostMapping
	public ItemDto insert(@RequestBody ItemDto dto) {
		ItemEntity itemEntity = convertToEntity(dto);
		itemRepository.save(itemEntity);
		return dto;
	}
	
	private ItemEntity convertToEntity(ItemDto dto) {
		ItemEntity itemEntity = new ItemEntity();
		itemEntity.setIdItemEntity(dto.getIdItemDto());
		itemEntity.setNamaItemEntity(dto.getNamaItemDto());
		itemEntity.setJumlahItemEntity(dto.getJumlahItemDto());
		
		if(kategoryItemRepository.findById(dto.getIdKatItemDto()).isPresent()) {
			KategoriItemEntity kategoriItemEntity = kategoryItemRepository.findById(dto.getIdKatItemDto()).get();
			itemEntity.setKategoriItemEntity(kategoriItemEntity);
		}
		return itemEntity;
	}
	
	@PutMapping("/{id}")
	public ItemDto update(@RequestBody ItemDto newItem, @PathVariable Integer id) {
		if(itemRepository.findById(id).isPresent()) {
			ItemDto itemDto = convertToDto(itemRepository.findById(id).get());
			itemDto.setNamaItemDto(newItem.getNamaItemDto());
			ItemEntity itemEntity = new ItemEntity();
			itemEntity= convertToEntity(itemDto);
			itemRepository.save(itemEntity);
			return itemDto;
			
		}
		return null;
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Integer id) {
		itemRepository.deleteById(id);
	}
	
	
	private ItemDto convertToDto(ItemEntity itemEntity) {
		ItemDto itemDto = new ItemDto();
		itemDto.setIdItemDto(itemEntity.getIdItemEntity());
		itemDto.setNamaItemDto(itemEntity.getNamaItemEntity());
		itemDto.setJumlahItemDto(itemEntity.getJumlahItemEntity());
		itemDto.setIdKatItemDto(itemEntity.getKategoriItemEntity().getIdKategoriEntity());
		return itemDto;
	}
}
