package gudang.aplikasi_gudang.model.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gudang.aplikasi_gudang.model.entity.ItemEntity;

@Repository
public interface ItemRepository extends JpaRepository<ItemEntity, Integer> {
	List<ItemEntity> findAllByKategoriItemEntityIdKategori(Integer idKategori);
}
