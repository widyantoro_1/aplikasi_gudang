package gudang.aplikasi_gudang.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import gudang.aplikasi_gudang.model.entity.KategoriItemEntity;
@Repository
public interface  KategoryItemRepository extends JpaRepository<KategoriItemEntity, Integer> {

}