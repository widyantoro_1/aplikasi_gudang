package gudang.aplikasi_gudang.model.dto;

public class KategoriItemDto {
	private Integer idKategori;
	private String namaKategori;
	public Integer getIdKategoriDto() {
		return idKategori;
	}
	public void setIdKategoriDto(Integer idKategori) {
		this.idKategori = idKategori;
	}
	public String getNamaKategoriDto() {
		return namaKategori;
	}
	public void setNamaKategoriDto(String namaKategori) {
		this.namaKategori = namaKategori;
	}
	
}
