package gudang.aplikasi_gudang.model.dto;

public class ItemDto {
	private Integer id;
	private String item;
	private Integer idKat;
	private Integer jumlah;
	
	public Integer getIdItemDto() {
		return id;
	}
	public void setIdItemDto(Integer id) {
		this.id = id;
	}
	public String getNamaItemDto() {
		return item;
	}
	public void setNamaItemDto(String item) {
		this.item = item;
	}
	public Integer getIdKatItemDto() {
		return idKat;
	}
	public void setIdKatItemDto(Integer idKat) {
		this.idKat = idKat;
	}
	public Integer getJumlahItemDto() {
		return jumlah;
	}
	public void setJumlahItemDto(Integer jumlah) {
		this.jumlah = jumlah;
	}
	
}
