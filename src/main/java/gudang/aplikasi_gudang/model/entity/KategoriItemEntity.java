package gudang.aplikasi_gudang.model.entity;

import javax.persistence.*;

@Entity
@Table(name="t_kategori")
public class KategoriItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idkategori", length = 25)
	private Integer idKategori;
	
	@Column(name = "namakategori")
	private String namaKategori;
	

	public Integer getIdKategoriEntity() {
		return idKategori;
	}

	public void setIdKategoriEntity(Integer idKategori) {
		this.idKategori = idKategori;
	}

	public String getNamaKategoriEntity() {
		return namaKategori;
	}

	public void setNamaKategoriEntity(String namaKategori) {
		this.namaKategori = namaKategori;
	}

}
