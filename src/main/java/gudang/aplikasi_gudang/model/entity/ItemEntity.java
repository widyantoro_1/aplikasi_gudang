package gudang.aplikasi_gudang.model.entity;

import javax.persistence.*;


@Entity
@Table(name="t_Item")
public class ItemEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "iditem",length = 25)
	private Integer idItem;
	
	@Column(name = "namaitem")
	private String namaItem;
	
	@ManyToOne
	@JoinColumn(name="idkategori")
	private KategoriItemEntity kategoriItemEntity;
	
	@Column(name = "jumlahitem")
	private Integer jumlahItem;

	public Integer getIdItemEntity() {
		return idItem;
	}

	public void setIdItemEntity(Integer idItem) {
		this.idItem = idItem;
	}

	public String getNamaItemEntity() {
		return namaItem;
	}

	public void setNamaItemEntity(String namaItem) {
		this.namaItem = namaItem;
	}

	public KategoriItemEntity getKategoriItemEntity() {
		return kategoriItemEntity;
	}

	public void setKategoriItemEntity(KategoriItemEntity kategoriItemEntity) {
		this.kategoriItemEntity = kategoriItemEntity;
	}

	public Integer getJumlahItemEntity() {
		return jumlahItem;
	}

	public void setJumlahItemEntity(Integer jumlahItem) {
		this.jumlahItem = jumlahItem;
	}

}
